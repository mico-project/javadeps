package eu.mico.platform;

import com.beust.jcommander.JCommander;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.*;
import java.lang.String;

/**
 * Small tool to find class names given packages or depended classes.
 *
 */
public class JavaDeps {
    static private JavaDepsCLI cli = null;

    public static void main(String[] args) throws Exception {

        //when run outside and IDE we must explicitly add the boot class loader jars to the classpath
        // in order to get default classes such as java.lang.* covered
        String bootClassPath = ManagementFactory.getRuntimeMXBean().getBootClassPath();
        for (String boot_cp : Arrays.asList(bootClassPath.split(":"))) {
            File f = new File(boot_cp);
            if (f.exists())
                ClassPathAppender.addFile(boot_cp);
        }

        try {
            cli = new JavaDepsCLI();
            JCommander cmd = new JCommander(cli, args);

            if (cli.isHelp()) {
                cmd.usage();
                System.exit(1);
            }

        } catch (com.beust.jcommander.ParameterException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }

        for (String cp : cli.getClassPaths()) {
            ClassPathAppender.addFile(cp);
        }


        Set<Class<?>> classes = new HashSet<Class<?>>();
        if (!cli.getUseClasses().isEmpty()) {
            for (String currClassName : cli.getUseClasses()) {
                classes.addAll(listClassesFromDependencyChecker(currClassName, cli.getPrefixes()));
            }
        } else {
            classes = listAllClassesFromPackage(cli.getPrefixes());
        }

        if (cli.getOutputFile() != null) {
            writeClassesToFile(classes, cli.getOutputFile());
        } else {
            printClassesPlain(classes);
        }

    }

    public static Set<Class<?>> listAllClassesFromPackage(List<String> packagePrefixes) throws IOException {
        Set<Class<?>> classes = new HashSet<Class<?>>();

        for (String packageName : packagePrefixes) {
            Reflections reflections = new Reflections(packageName, new SubTypesScanner(false));

            Set<Class<? extends Object>> allClassesForPackage =
                    reflections.getSubTypesOf(Object.class);

            classes.addAll(allClassesForPackage);
        }

        return classes;
    }

    public static Set<Class<?>> listClassesFromDependencyChecker(String className, List<String> packagePrefixes) throws Exception {
        Set<Class<?>> classes = new HashSet<Class<?>>();

        for (String prefix : packagePrefixes) {
            classes.addAll(ClassCollector.getClassesUsedBy(className, prefix));
        }

        return classes;
    }

    private static void printClassesPlain(Collection<Class<?>> classes) {
        List<String> list = new ArrayList<String>();
        for (final Class<?> cls : classes) {
            list.add(cls.getName());
        }
        Collections.sort(list);

        for (final String cls : list) {
            if (cli.isSkipNested()) {
                if (cls.contains("$"))
                    continue;

            }
            System.out.println(cls);
        }
    }

    private static void writeClassesToFile(Collection<Class<?>> classes, String filePath) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(cli.getOutputFile()));
        List<String> list = new ArrayList<String>();
        for (final Class<?> cls : classes) {
            list.add(cls.getName());
        }
        Collections.sort(list);

        for (final String cls : list) {
            if (cli.isSkipNested()) {
                if (cls.contains("$"))
                    continue;
            }
            writer.write(cls);
            writer.newLine();
        }
        writer.close();
    }
}