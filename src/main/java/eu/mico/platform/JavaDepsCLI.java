package eu.mico.platform;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christian on 3/8/16.
 */
public class JavaDepsCLI {

    @Parameter(names = {"--packages", "-p"}, description = "Package prefixes to be considered.", required = true, variableArity = true)
    private List<String> prefixes = new ArrayList<String>();

    @Parameter(names = {"--classpath", "-cp"}, description = "Adds a classpath to be searched for classes.")
    private List<String> classPaths = new ArrayList<String>();

    @Parameter(names = {"--used-by", "-u"}, description = "Only classes used by the given classes will be printed. Use --packages parameter to limit scope.")
    private List<String> useClasses = new ArrayList<String>();

    @Parameter(names = {"--help", "-h"}, description = "Prints this help", help = true)
    private boolean help;

    @Parameter(names = {"--skip-inner", "-s"}, description = "Skips inner classes in output")
    private boolean skipNested;

    @Parameter(names = {"--output-file", "-o"}, description = "File to write the classes to. If not specified classes are written to stdout.")
    private String outputFile;


    public boolean isHelp() {
        return help;
    }


    public List<String> getPrefixes() {
        return prefixes;
    }

    public List<String> getClassPaths() {
        return classPaths;
    }

    public List<String> getUseClasses() {
        return useClasses;
    }

    public boolean isSkipNested() {
        return skipNested;
    }

    public String getOutputFile() {return outputFile; }
}
